package padronelectoral;

import org.junit.Test;
import static org.junit.Assert.*;

import java.time.LocalDate;

public class PersonaTest {
    @Test public void comprobarNuevaPersona() {
        Persona persona = new Persona(23111111,"Perez",LocalDate.parse("1974-05-23"));
        
        assertEquals("Perez",persona.getApellido());
    }
    @Test 
    public void verificarCalculoDeEdad() {
        Persona persona = new Persona(23111111,"Perez",LocalDate.parse("1974-05-23"));
        assertEquals(Integer.valueOf(50),persona.edad());
    }

    @Test 
    public void verificarEquals() {
        Persona persona = new Persona(23111111,"Perez",LocalDate.parse("1974-05-23"));
        Persona nuevaPersona = new Persona(23111111,"Perez",LocalDate.parse("1974-05-23"));
        assertEquals(true,persona.equals(nuevaPersona));
    }

}