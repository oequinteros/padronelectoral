package padronelectoral;

import org.junit.Test;
import static org.junit.Assert.*;

import java.time.LocalDate;

public class PadronTest {
    @Test public 
    void comprobarNuevoPadron() {
        Padron padron = new Padron();        
        assertEquals(0,padron.getNomina().size());
    }
    @Test public 
    void agregarUnaPersonaAlPadron() {
        Padron padron = new Padron();    
        Persona persona = new Persona(23111111,"Perez",LocalDate.parse("1974-05-23"));
        padron.agregarPersona(persona);        
        assertEquals(1,padron.getNomina().size());
    }
    @Test 
    public void eliminarUnaPersonaDelPadron() throws PersonaInexistenteException {
        Padron padron = new Padron();    
        Persona persona = new Persona(23111111,"Perez",LocalDate.parse("1974-05-23"));
        padron.agregarPersona(persona);          
        padron.eliminarPersona(23111111);
        assertEquals(0,padron.getNomina().size());
    }
    @Test public 
    void modificarUnaPersonaAlPadron() throws PersonaInexistenteException {
        Padron padron = new Padron();    
        Persona persona = new Persona(23111111,"Perez",LocalDate.parse("1974-05-23"));
        padron.agregarPersona(persona);
        Persona nuevaPersona = new Persona(23111111,"Juan Gonzalez",LocalDate.parse("1974-05-23"));          
        padron.modificarPersona(nuevaPersona);
        assertEquals("Juan Gonzalez",padron.getPersona(23111111).getApellido());
    }

    @Test (expected = PersonaInexistenteException.class)
    public void buscarPersonaInexistenteEnElPadron() throws PersonaInexistenteException {
        Padron padron = new Padron();    
        Persona persona = new Persona(23111111,"Perez",LocalDate.parse("1974-05-23"));
        padron.agregarPersona(persona);       
        Persona personaBuscada = padron.getPersona(22111111);
        assertNotNull(personaBuscada);
    }

}