package padronelectoral;

import java.time.LocalDate;

public class PoderJudicial {
    Padron padron = new Padron();    

    public void IngresarVotante(){
        Persona persona = new Persona(2345678,"Quinteros",LocalDate.parse("1974-05-23"));
        padron.agregarPersona(persona);        
    }

    public void EliminarVotante(){
        try {
            padron.eliminarPersona(2345677);
        } catch (PersonaInexistenteException e) {
            System.out.print("No se puede eliminar");
        }        
    }
    
}
