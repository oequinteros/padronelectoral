package padronelectoral;

import java.time.LocalDate;
import java.time.Period;

class Persona {
    private Integer dni;
    private String apellido;
    private LocalDate fechaNacimiento;

    public Persona(Integer dni, String apellido, LocalDate fechaNacimiento) {
        this.setDni(dni);
        this.setApellido(apellido);
        this.setFechaNacimiento(fechaNacimiento);
        if (this.edad()<16) {
            throw new PersonaMenorEdadException();
        }        
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getDni() {
        return dni;
    }

    public void setDni(Integer dni) {
        this.dni = dni;
    }
    public Integer edad(){
        LocalDate hoy = LocalDate.now();
        Period periodo = Period.between(fechaNacimiento,hoy);
        return periodo.getYears();
    }
    @Override
    public boolean equals(Object o) {
        
        Persona aComparar = (Persona)o;

        return this.getDni().equals(aComparar.getDni());
    }    
}
