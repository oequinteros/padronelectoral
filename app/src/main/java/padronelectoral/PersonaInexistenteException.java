package padronelectoral;

class PersonaInexistenteException extends Exception {
    public PersonaInexistenteException(){
        super("Persona Inexistente");
    }
}