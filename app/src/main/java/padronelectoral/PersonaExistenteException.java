package padronelectoral;

public class PersonaExistenteException extends RuntimeException {
    public PersonaExistenteException(){
        super("La Persona ya exite en la base de datos");
    }
}