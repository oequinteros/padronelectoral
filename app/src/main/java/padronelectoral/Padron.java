package padronelectoral;

import java.util.ArrayList;

class Padron {
    private ArrayList<Persona> nomina;

    public Padron() {
        setNomina(new ArrayList<Persona>());
    }

    public ArrayList<Persona> getNomina() {
        return nomina;
    }

    public void setNomina(ArrayList<Persona> nomina) {
        this.nomina = nomina;
    }

    public void agregarPersona(Persona persona) throws PersonaExistenteException {
        for (Persona var : nomina) {
            if (var.equals(persona)) {
                throw new PersonaExistenteException();
            }        
        }
        this.nomina.add(persona);
    }

    public void eliminarPersona(Integer dni) throws PersonaInexistenteException{
        
        Persona personaEncontrada = getPersona(dni);
        
        nomina.remove(personaEncontrada);
    }

    public Persona getPersona(Integer dni) throws PersonaInexistenteException{
        Persona personaEncontrada = null;
        for (Persona var : nomina) {
            if (var.getDni().equals(dni)) {
                personaEncontrada = var;
                break;
            }
        }
        if (personaEncontrada == null) {
            throw new PersonaInexistenteException();
        }    
        return personaEncontrada;
    }

    public void modificarPersona(Persona nuevaPersona) throws PersonaInexistenteException {
        Persona personaEncontrada;
        personaEncontrada = getPersona(nuevaPersona.getDni());
        nomina.remove(personaEncontrada); 
        nomina.add(nuevaPersona);               
	}
    
}